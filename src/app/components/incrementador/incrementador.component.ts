import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styleUrls: ['./incrementador.component.css']
})
export class IncrementadorComponent implements OnInit {

  @Input() procentaje: number = 50;
  @Input('nombre') leyenda: string = 'Leyendas';
  @Output() cambioValor: EventEmitter<number> = new EventEmitter();
  @ViewChild('txtProgres') txtProgres: ElementRef;

  constructor() {
  }

  ngOnInit() {
  }
  onChange(newValor: number ) {
    if (newValor >= 100) {
this.procentaje = 100;
    } else if (newValor <= 0) {
      this.procentaje = 0;
    } else {
       this.procentaje = newValor;
    }
    this.procentaje = this.procentaje + newValor;
    this.cambioValor.emit(this.procentaje);
  

  }
  cambiar(valor) {
    if (this.procentaje >= 100 && valor > 0) {
      this.procentaje = 100;
      return;
    }
    if (this.procentaje <= 0 && valor < 0) {
      this.procentaje = 0;
      return;
    }
    this.procentaje = this.procentaje + valor;
    this.cambioValor.emit(this.procentaje);
    this.txtProgres.nativeElement.focus();
  }

}
