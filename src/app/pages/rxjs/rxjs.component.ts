import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { retry, map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: []
})
export class RxjsComponent implements OnInit, OnDestroy {

  subcription: Subscription;
  constructor() {
   this.subcription = this.regresaObservable().
    subscribe(
      numero => console.log('sub', numero),
        error => console.log('Error obs', error),
          () => console.log('el obs termino')
    );

  }

  ngOnInit() {
  }
  ngOnDestroy(){
this.subcription.unsubscribe();
  }

  regresaObservable(): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      const contador = 0;
      const inter = setInterval(() => {
        contador += 1;
        const salida = {
          valor: contador
        };
        observer.next( salida);
       // if (contador === 3) {
       //   clearInterval(inter);
       //   observer.complete();
      //  }
      //  if (contador === 2) {
         // clearInterval(inter);
       //   observer.error('Auxilio');
       // }
      }, 1000);
    }).pipe(
      map( resp => resp.valor ),
      filter( ( valor, index) => {
        //console.log('filter', valor, index);
        if ( (valor % 2) === 1){
          return true;
        } else {
          return false;
        }

      })
    );


  }

}
