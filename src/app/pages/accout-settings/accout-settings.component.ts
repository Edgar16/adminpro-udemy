import { Component, OnInit, Inject, ElementRef } from '@angular/core';
import { SettingsService } from '../../services/service.index';


@Component({
  selector: 'app-accout-settings',
  templateUrl: './accout-settings.component.html',
  styles: []
})
export class AccoutSettingsComponent implements OnInit {

  constructor(  public ajustes: SettingsService ) { }

  ngOnInit() {
    this.colocarCheck();
  }

  CambiarColor( tema: string, link: any) {
    console.log(link);
    this.aplicarCheck(link);
    this.ajustes.aplicartema(tema);

  }

  colocarCheck() {
    const selectores: any = document.getElementsByClassName('selector');
    const tema = this.ajustes.ajustes.tema;
    for (const clase  of selectores) {
      if( clase.getAttribute('data-theme') === tema) {
      clase.classList.add('working');
        break;
      }
    }
  }

  aplicarCheck(link: any) {
    const selectores: any = document.getElementsByClassName('selector');
    const tema = this.ajustes.ajustes.tema;
    for ( const clase of selectores) {
        clase.classList.remove('working');
      //  break;
      }
      link.classList.add('working');
    }
  }