import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ProgressComponent } from './progress/progress.component';
import { Graficas1Component } from './graficas1/graficas1.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../login/register.component';
import { NopagefoundComponent } from '../nopagefound/nopagefound.component';
import { AccoutSettingsComponent } from './accout-settings/accout-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';



const pagesRoutes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: 'progress', component: ProgressComponent, data: { titulo: 'Bahrrras de Progreso'} },
            { path: 'graficas1', component: Graficas1Component, data: { titulo: 'Graficos' }  },
            { path: 'dashboard', component: DashboardComponent, data: { titulo: 'Inicio' }  },
            { path: 'promesas', component: PromesasComponent, data: { titulo: 'Barras de Progreso' }  },
            { path: 'rxjs', component: RxjsComponent, data: { titulo: 'RsJs' }  },
            { path: 'account-settings', component: AccoutSettingsComponent, data: { titulo: 'Ajustes del Tema' }  },
            { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
        ]
    },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: '**', component: NopagefoundComponent }
];

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes);