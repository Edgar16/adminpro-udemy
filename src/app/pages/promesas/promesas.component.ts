import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: []
})
export class PromesasComponent implements OnInit {

  constructor() {
    this.contartres().then( 
      m => console.log('termino' , m)
    ).catch( error => console.error('error en la promesa', error));
  }

  ngOnInit() {
  }

  contartres(): Promise<boolean> {
    return  new Promise((resolve, reject) => {
      const contador = 0;
      const intervalo = setInterval(() => {
        console.log('Contador: ', contador);
        contador += 1;
        if (contador === 3) {
          resolve( true);
          clearInterval(intervalo);
        }
      }, 1000);
    });
  }


}
