 import { Injectable } from '@angular/core';
import { Component, OnInit, Inject, ElementRef } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
@Injectable({
  providedIn: 'root'
})
export class SettingsService {
ajustes: Ajustes = {
  temaURL: 'assets/css/colors/default.css',
  tema: 'default'
}
  constructor(@Inject(DOCUMENT) private _documento ) {
    this.cargarajustes();
  }

  guardarajustes() {
   localStorage.setItem('ajustes', JSON.stringify(this.ajustes));
  }

  cargarajustes() {
    if (localStorage.getItem('ajustes')) {
      this.ajustes = JSON.parse(localStorage.getItem('ajustes'));
      this.aplicartema( this.ajustes.tema );
    }
  }

  aplicartema( tema: string){
    let url = `assets/css/colors/${tema}.css`;
    this._documento.getElementById('tema').setAttribute('href', url);
    this.ajustes.tema = tema;
    this.ajustes.temaURL = url;

    this.guardarajustes();
  }
}




  interface Ajustes {
    temaURL: string;
    tema: string;

  }

