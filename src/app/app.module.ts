import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AAP_ROUTES } from './app.routes';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
import { PagesModule } from './pages/pages.module';
import { FormsModule } from '@angular/forms';
import { SettingsService } from './services/service.index';
import { ServiceModule } from './services/service.module';
// modulos



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
  ],
  imports: [BrowserModule,
    AAP_ROUTES,
    PagesModule,
    FormsModule,
    ServiceModule],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule {}
