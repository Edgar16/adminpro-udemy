import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BradcrumbsComponent } from './bradcrumbs/bradcrumbs.component';
import { NopagefoundComponent } from '../nopagefound/nopagefound.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    RouterModule,
    CommonModule
  ],
  declarations: [
    NopagefoundComponent,
    HeaderComponent,
    SidebarComponent,
    BradcrumbsComponent
  ],
  exports: [
    HeaderComponent,
    SidebarComponent,
    BradcrumbsComponent,
    NopagefoundComponent
  ]
})
export class SharedModule {}